elpa-undo-tree (0.8.1-4) UNRELEASED; urgency=medium

  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.0, no changes needed.
  * Remove constraints unnecessary since buster:
    + elpa-undo-tree: Drop versioned constraint on emacs in Recommends.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 27 Aug 2022 05:14:38 -0000

elpa-undo-tree (0.8.1-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:08:57 +0900

elpa-undo-tree (0.8.1-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.3.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 11:32:02 +0900

elpa-undo-tree (0.8.1-1) unstable; urgency=medium

  [ Aymeric Agon-Rambosson ]
  * New upstream release.
  * Adopt the package from David Krauser (Closes: #981118).
  * Bump debhelper compat to 13.
  * Bump Standards version to 4.5.1 (no changes needed).
  * Update copyright years.
  * Update upstream URL in d/watch.

  [ Nicholas D Steeves ]

  * Drop emacs24 from Enhances (package does not exist in bullseye).
  * Update Homepage and Source for new upstream location.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Sun, 31 Jul 2022 05:12:46 +0200

elpa-undo-tree (0.7.4-1) unstable; urgency=medium

  [ David Krauser ]
  * Update to upstream version 0.7.4 (Closes: #952458)
    - Fix argument order in gv setter definitions.
    - Switch from cl to cl-lib.
    - Fix hook function issue in Emacs 27.
    - Fix some byte-compilation warnings.
    - Don't attempt to save undo history if history file is unwritable.
    - undo-outer-limit can also be null (no limit) in recent Emacsen.

  [ Nicholas D Steeves ]
  * Enable autopkgtests.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 24 Feb 2020 14:46:45 -0500

elpa-undo-tree (0.7.1-1) unstable; urgency=medium

  [ David Krauser ]
  * Update to upstream version 0.7.1
  * The upstream repository does not support shallow clones, so add
    gitmode=full to watch file to force full git clones
  * Update upstream homepage URLs
  * Update package Standards-Version to 4.4.1
    - Drop Built-Using
  * Update package Standards-Version to 4.5.0 (no changes required)
  * Switch to debhelper-compat 12
  * Drop dh-elpa version qualifier
    - Version 1.6 of dh-elpa is the oldest version we have in the repositories,
      which is newer than the minimum version required to build this package.
      Should be safe to remove the qualifier.
  * Add Rules-Requires-Root: no
  * Configure gbp to use pristine-tar.
  * Switch to xz compression for source tarballs.
  * Remove unnecessary --parallel argument to dh
    - This argument is now enabled by default
  * Add David Krauser to uploaders (Closes: #915491)
  * Update maintainer name and email address
  * Drop Dmitry Bogatov from uploaders

  [ Debian Janitor ]
  * Use canonical URL in Vcs-Browser.

 -- David Krauser <david@krauser.org>  Thu, 13 Feb 2020 22:20:26 -0500

elpa-undo-tree (0.6.4-4) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.16
  * Migrate Vcs-* to salsa

 -- David Bremner <bremner@debian.org>  Sun, 01 Sep 2019 09:25:24 -0300

elpa-undo-tree (0.6.4-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Sat, 02 Jun 2018 20:12:33 -0300

elpa-undo-tree (0.6.4-2) UNRELEASED; urgency=medium

  * Team upload.
  * Correct Homepage: in d/control and Source: in d/copyright.

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 20 Jun 2016 18:40:30 +0900

elpa-undo-tree (0.6.4-1) unstable; urgency=medium

  * Initial release. (Closes: #827679)

 -- Dmitry Bogatov <KAction@gnu.org>  Tue, 14 Jun 2016 14:53:42 +0300
